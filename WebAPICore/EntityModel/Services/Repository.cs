﻿using EntityModel.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EntityModel.Services
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private zModel context = null;

        public Repository(zModel db)
        {
            context = db;
        }

        public List<T> GetItems()
        {
            return context.Set<T>().ToList();
        }

        public T FindItem(object id)
        {
            return context.Set<T>().Find(id);
        }

        public void AddItem(T item)
        {
            if (item == null)
                throw new ArgumentNullException();

            context.Set<T>().Add(item);
        }

        public void AddItems(params T[] items)
        {
            if (items == null || items.Length == 0)
                throw new ArgumentNullException();

            context.Set<T>().AddRange(items);
        }

        public void UpdateItem(T item)
        {
            if (item == null)
                throw new ArgumentNullException();

            context.Set<T>().Update(item);
        }

        public void UpdateItems(params T[] items)
        {
            if (items == null || items.Length == 0)
                throw new ArgumentNullException();

            context.Set<T>().UpdateRange(items);
        }

        public void RemoveItem(T item)
        {
            if (item == null)
                throw new ArgumentNullException();

            context.Set<T>().Remove(item);
        }

        public void RemoveItems(params T[] items)
        {
            if (items == null || items.Length == 0)
                throw new ArgumentNullException();

            context.Set<T>().RemoveRange(items);
        }

        public async Task<List<T>> GetItemsAsync()
        {
            return await context.Set<T>().ToListAsync();
        }

        public async Task<T> FindItemAsync(object id)
        {
            return await context.Set<T>().FindAsync(id);
        }

        public async Task AddItemAsync(T item)
        {
            if (item == null)
                throw new ArgumentNullException();

          await  context.Set<T>().AddAsync(item);
        }

        public async Task AddItemsAsync(params T[] items)
        {
            if (items == null || items.Length == 0)
                throw new ArgumentNullException();

           await context.Set<T>().AddRangeAsync(items);
        }

        //public Repository()
        //{
        //    context = new zModel();
        //}

        //public Repository(zModel db)
        //{
        //    context = db;
        //}

        //public List<T> GetItems()
        //{
        //    return context.Set<T>().ToList();
        //}

        //public async Task<List<T>> GetItemsAsync()
        //{
        //    return await context.Set<T>().ToListAsync();
        //}

        //public T FindItem(object ID)
        //{
        //    return context.Set<T>().Find(ID);
        //}

        //public async Task<T> FindItemAsync(object ID)
        //{
        //    return await context.Set<T>().FindAsync(ID);
        //}

        //public void AddOrUpdate(T item)
        //{
        //    if (item == null)
        //        throw new ArgumentNullException();

        //    context.Set<T>().AddOrUpdate(item);
        //}

        //public void AddOrUpdate(params T[] items)
        //{
        //    if (items == null || items.Length == 0)
        //        throw new ArgumentNullException();

        //    context.Set<T>().AddOrUpdate(items);
        //}

        //public void Remove(T item)
        //{
        //    if (item == null)
        //        throw new ArgumentNullException();

        //    context.Set<T>().Remove(item);
        //}

        //public void Remove(params T[] items)
        //{
        //    if (items == null || items.Length == 0)
        //        throw new ArgumentNullException();

        //    context.Set<T>().RemoveRange(items);
        //}
    }
}
