﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntityModel.Services
{
    public interface IRepository<T> where T : class, new()
    {
        List<T> GetItems();
        Task<List<T>> GetItemsAsync();

        T FindItem(object id);
        Task<T> FindItemAsync(object id);

        void AddItem(T item);
        void AddItems(params T[] items);
        Task AddItemAsync(T item);
        Task AddItemsAsync(params T[] items);

        void UpdateItem(T item);
        void UpdateItems(params T[] items);

        void RemoveItem(T item);
        void RemoveItems(params T[] items);
    }
}
