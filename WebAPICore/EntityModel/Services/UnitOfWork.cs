﻿using EntityModel.Context;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntityModel.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private Dictionary<string, object> repositories;
        private zModel Context;

        public UnitOfWork(zModel context)
        {
            Context = context;
        }

        public void BeginTransaction()
        {
            Context.Database.BeginTransaction();
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public void CommitTransaction()
        {
            if (Context.Database.CurrentTransaction != null)
                Context.Database.CurrentTransaction.Commit();
        }

        public void RollbackTransaction()
        {
            if (Context.Database.CurrentTransaction != null)
                Context.Database.CurrentTransaction.Rollback();
        }

        public IRepository<T> GetRepository<T>() where T : class, new()
        {
            if (repositories == null)
                repositories = new Dictionary<string, object>();

            string type = typeof(T).Name;

            if (!repositories.ContainsKey(type))
            {
                var repositoryType = typeof(Repository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), Context);
                repositories.Add(type, repositoryInstance);
            }
            return (Repository<T>)repositories[type];
        }
    }
}