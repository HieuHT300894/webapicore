﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel.General
{
    public class Detail
    {
        public int KeyID { get; set; } = AutoGenerateID.KeyID;
        public string Note { get; set; } = string.Empty;
        public int Status { get; set; } = 0;
        public bool IsDefault { get; set; } = false;
    }
}
