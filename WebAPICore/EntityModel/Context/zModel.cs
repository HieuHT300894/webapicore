﻿using Common;
using EntityModel.EF;
using Microsoft.EntityFrameworkCore;

namespace EntityModel.Context
{
    public class zModel : DbContext
    {
        /*
         * Add-Migration db1 -context zModel
         * Update-Database -context zModel
         * Remove-Migration -context zModel
         */

        public zModel() : base()
        {
        }
        public zModel(DbContextOptions<zModel> options) : base(options)
        {
        }

        public DbSet<xAccount> xAccounts { get; set; }
        public DbSet<xAgency> xAgencies { get; set; }
        public DbSet<xDisplay> xDisplays { get; set; }
        public DbSet<xHistory> xHistories { get; set; }
        public DbSet<xLanguage> xLanguages { get; set; }
        public DbSet<xPermission> xPermissions { get; set; }
        public DbSet<xPermissionDetail> xPermissionDetails { get; set; }
        public DbSet<xPermissionGroup> xPermissionGroups { get; set; }
        public DbSet<xPersonnel> xPersonnels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region PrimaryKey
            modelBuilder.Entity<xAccount>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xAccount>().Property(x => x.KeyID).ValueGeneratedNever();
            modelBuilder.Entity<xAgency>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xPersonnel>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xDisplay>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xPermissionGroup>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xLanguage>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xPermission>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xPermissionDetail>().HasKey(x => x.KeyID);
            modelBuilder.Entity<xHistory>().HasKey(x => x.KeyID);
            #endregion

            #region Ignore
            modelBuilder.Entity<xPermission>().Ignore(x => x.Code);
            modelBuilder.Entity<xPermission>().Ignore(x => x.Name);
            modelBuilder.Entity<xPermission>().Ignore(x => x.CreatedDate);
            modelBuilder.Entity<xPermission>().Ignore(x => x.CreatedBy);
            modelBuilder.Entity<xPermission>().Ignore(x => x.ModifiedDate);
            modelBuilder.Entity<xPermission>().Ignore(x => x.Note);

            modelBuilder.Entity<xAccount>().Ignore(x => x.Code);
            modelBuilder.Entity<xAccount>().Ignore(x => x.Name);

            modelBuilder.Entity<xHistory>().Ignore(x => x.Code);
            modelBuilder.Entity<xHistory>().Ignore(x => x.Name);
            modelBuilder.Entity<xHistory>().Ignore(x => x.CreatedDate);
            modelBuilder.Entity<xHistory>().Ignore(x => x.CreatedBy);
            modelBuilder.Entity<xHistory>().Ignore(x => x.ModifiedDate);
            modelBuilder.Entity<xHistory>().Ignore(x => x.ModifiedBy);
            modelBuilder.Entity<xHistory>().Ignore(x => x.Note);
            #endregion
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(Define.Instance.ConnectionString);
        }
    }
}